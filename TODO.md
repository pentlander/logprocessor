* Add support for structured queries (i.e. querying json fields)
* Add custom error types instead of anyhow
* Create cli for querying
* Add block compression
* Investigate hybrid row-column format
* Separate query/ingest functionality into separate services (still in same binary)
* Add disk disk caching for faster queries
* Add block compaction and cleanup
* Support ingest from multiple applications simultaneously
* Investigate indexing options (bloom filter, fst, more rocksdb index fields)
* Add benchmarking
* Add load testing harness
* Add metrics for query perf, ingest bytes, etc.
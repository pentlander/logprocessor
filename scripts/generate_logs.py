import uuid
import json
import random
import time


def main():
    random.seed(255)
    while True:
        data = {'request_id': str(uuid.uuid4()),
                'message': f'This is a message: {random.randrange(100)}'}
        print(json.dumps(data), end='', flush=True)
        time.sleep(0.5)


if __name__ == '__main__':
    main()

use std::str::FromStr;

use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::entry::LogEntry;
use crate::AppKey;

pub mod storage;

/// Contains block metadata
#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct BlockHeader {
    /// Current version of the block schema
    version: u8,
    /// Size of the block measured in bytes
    size: u64,
    /// Earliest log entry timestamp within the block
    min_time: OffsetDateTime,
    /// Latest log entry timestamp within the block
    max_time: OffsetDateTime,
    /// List entry offsets
    offsets: Vec<EntryOffset>,
}

/// An offset for a range of entries that starts at min_time and ends at max_time
#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq)]
pub struct EntryOffset {
    min_time: OffsetDateTime,
    max_time: OffsetDateTime,
    offset: u64,
}

/// A block is the structure that gets serialized and placed into the persistent data store.
/// The block contains a header that enables the reconstruction of the index in the event the index
/// is lost, as well as information to facilitate partial data fetches.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Block {
    /// Header that contains metadata about the block
    header: BlockHeader,

    // Entries serialized as a msgpack list of entries directly from fluentbit, i.e. [[timestamp, log_entry], ...]
    /// Vec of log entries
    log_entries: Vec<LogEntry>,
}

impl Block {
    pub fn new(start_time: OffsetDateTime) -> Block {
        Block {
            header: BlockHeader {
                version: 0,
                size: 0,
                min_time: start_time.clone(),
                max_time: start_time,
                offsets: Vec::new(),
            },
            log_entries: Vec::new(),
        }
    }

    pub fn header(&self) -> &BlockHeader {
        &self.header
    }

    pub fn entries(&self) -> &[LogEntry] {
        &self.log_entries
    }

    pub fn into_entries(self) -> Vec<LogEntry> {
        self.log_entries
    }

    pub fn min_time(&self) -> &OffsetDateTime {
        &self.header.min_time
    }

    pub fn max_time(&self) -> &OffsetDateTime {
        &self.header.max_time
    }

    /// Add a log entry to the block. If the timestamp for this entry is greater than the
    /// current max timestamp in the header, update the max timestamp.
    // TODO: Add EntryOffsets when adding an entry as well. Or maybe that should occur in a finalize step?
    pub fn add_entry(&mut self, entry: LogEntry, size: u64) {
        if &entry.timestamp < &self.header.min_time {
            self.header.min_time = entry.timestamp;
        }
        if &entry.timestamp > &self.header.max_time {
            self.header.max_time = entry.timestamp;
        }
        let insert_idx = match self
            .log_entries
            .binary_search_by(|e| e.timestamp.cmp(&entry.timestamp))
        {
            Ok(idx) => idx,
            Err(idx) => idx,
        };
        self.log_entries.insert(insert_idx, entry);
        self.header.size += size;
    }

    pub fn size(&self) -> u64 {
        self.header.size
    }
}

// Safe separator char according to: https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingMetadata.html#object-key-guidelines-safe-characters
const OBJECT_KEY_SEPARATOR: &str = "!";

/// Key used to look up a `Block` in `BlockStorage`
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct BlockKey {
    pub min_time: OffsetDateTime,
    pub max_time: OffsetDateTime,
    pub app_key: AppKey,
    pub header_size: u64,
}

impl BlockKey {
    pub fn from_string(key_string: impl AsRef<str>) -> BlockKey {
        let key_str = key_string.as_ref();
        let mut split_key = key_str.split(OBJECT_KEY_SEPARATOR);
        let min_time =
            OffsetDateTime::from_unix_timestamp(i64::from_str(split_key.next().unwrap()).unwrap());
        let max_time =
            OffsetDateTime::from_unix_timestamp(i64::from_str(split_key.next().unwrap()).unwrap());
        let app_key = split_key.next().unwrap().into();
        let header_size = u64::from_str(split_key.next().unwrap()).unwrap();
        BlockKey {
            min_time,
            max_time,
            app_key,
            header_size,
        }
    }

    /// Convert a listing into an S3 object key. The key itself stores object metadata.
    pub fn to_string(&self) -> String {
        [
            self.min_time.timestamp().to_string(),
            self.max_time.timestamp().to_string(),
            self.app_key.clone(),
            u64::to_string(&self.header_size),
        ]
        .join(OBJECT_KEY_SEPARATOR)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn block_key_serde() {
        let block_key = BlockKey {
            min_time: OffsetDateTime::unix_epoch(),
            max_time: OffsetDateTime::unix_epoch() + time::Duration::seconds(30),
            app_key: "foo".into(),
            header_size: 514u64,
        };

        let block_key_str = block_key.to_string();
        let deserialized_block_key = BlockKey::from_string(block_key_str);

        assert_eq!(deserialized_block_key, block_key);
    }
}

use std::collections::HashMap;
use std::io;
use std::sync::Arc;

use async_trait::async_trait;
use kv_log_macro as log;
use tokio::sync::Mutex;

use crate::block::{Block, BlockHeader, BlockKey};
use crate::entry::LogEntry;
use crate::{AppKey, AppResult};

/// Handles storing and retrieving bytes from the specified path.
#[async_trait]
pub trait Storage: StorageClone + Send + Sync {
    /// Store bytes at the specified path.
    async fn put(&self, path: &str, data: &[u8]) -> AppResult<()>;

    // Retrieve bytes from the specified path.
    async fn get(&self, path: &str) -> AppResult<Vec<u8>>;
}

/// Trait that allows for object-safe cloning.
/// See: https://stackoverflow.com/questions/30353462/how-to-clone-a-struct-storing-a-boxed-trait-object/30353928
pub trait StorageClone {
    fn clone_box(&self) -> Box<dyn Storage>;
}

impl<T> StorageClone for T
where
    T: 'static + Storage + Clone,
{
    fn clone_box(&self) -> Box<dyn Storage> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Storage> {
    fn clone(&self) -> Self {
        self.clone_box()
    }
}

#[async_trait]
impl Storage for s3::Bucket {
    async fn put(&self, path: &str, data: &[u8]) -> AppResult<()> {
        self.put_object(path, data).await?;
        Ok(())
    }

    async fn get(&self, path: &str) -> AppResult<Vec<u8>> {
        Ok(self.get_object(path).await.map(|(data, _)| data)?)
    }
}

/// Stores and retrieves blocks from persistent storage.
#[derive(Clone)]
pub struct BlockStore {
    storage: Box<dyn Storage>,
}

impl BlockStore {
    /// Create a new `BlockStore` backed by the given `storage`.
    pub fn new(storage: Box<dyn Storage>) -> Self {
        BlockStore { storage }
    }

    /// Put a block into storage with the given app key.
    pub async fn put_block(&self, app_key: AppKey, block: &Block) -> AppResult<BlockKey> {
        let mut buf = Vec::new();
        rmp_serde::encode::write_named(&mut buf, &block.header)?;
        let header_size = buf.len();

        rmp_serde::encode::write_named(&mut buf, &block.log_entries)?;

        let block_key = BlockKey {
            app_key,
            min_time: *block.min_time(),
            max_time: *block.max_time(),
            header_size: header_size as u64,
        };

        let block_key_str = block_key.to_string();
        log::debug!("Putting object", { block_key: &block_key_str, num_bytes: buf.len() });
        self.storage.put(&block_key_str, &buf).await?;
        Ok(block_key)
    }

    pub async fn get_block(&self, block_key: &BlockKey) -> AppResult<Block> {
        let bytes = self.storage.get(&block_key.to_string()).await?;
        let mut cursor = io::Cursor::new(bytes);
        let header: BlockHeader = rmp_serde::decode::from_read(&mut cursor)?;
        let log_entries: Vec<LogEntry> = rmp_serde::decode::from_read(&mut cursor)?;
        Ok(Block {
            header,
            log_entries,
        })
    }
}

/// Simple in-memory implementation of `Storage`
#[derive(Default, Clone)]
pub struct VecStorage {
    map: Arc<Mutex<HashMap<String, Vec<u8>>>>,
}

#[async_trait]
impl Storage for VecStorage {
    async fn put(&self, path: &str, data: &[u8]) -> AppResult<()> {
        self.map
            .lock()
            .await
            .insert(path.to_string(), data.to_owned());
        Ok(())
    }

    async fn get(&self, path: &str) -> AppResult<Vec<u8>> {
        let map = self.map.lock().await;
        map.get(path)
            .map(Clone::clone)
            .ok_or(anyhow::Error::msg("Bytes do not exist at path"))
    }
}

#[cfg(test)]
mod tests {
    use crate::block::storage::{BlockStore, VecStorage};
    use crate::block::*;

    #[tokio::test]
    async fn get_block_deserializes_block() {
        let block = Block::new(OffsetDateTime::now_utc());
        let block_store = BlockStore::new(Box::new(VecStorage::default()));
        let block_key = block_store.put_block("foo".into(), &block).await.unwrap();

        let fetched_block = block_store.get_block(&block_key).await.unwrap();

        assert_eq!(fetched_block, block);
    }
}

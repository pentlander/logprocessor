use crate::AppKey;
use rmpv::Value;
use serde::{Deserialize, Serialize};
use std::array::TryFromSliceError;
use std::convert::{TryFrom, TryInto};
use time::{Duration, OffsetDateTime};

const APP_KEY: &str = "app_key";

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct LogEntry {
    pub timestamp: OffsetDateTime,
    pub app_key: AppKey,
    log_entry_value: rmpv::Value,
}

impl LogEntry {
    pub fn new(value: rmpv::Value) -> LogEntry {
        let mut array = match value {
            rmpv::Value::Array(vec) => vec,
            _ => panic!("Value is not array."),
        };
        assert_eq!(array.len(), 2, "Size of array must be 2.");
        let log_entry_value = array.pop().expect("Asserted size 2.");
        let timestamp_ext = array.pop().expect("Asserted size 2.");
        let (ext, bytes) = timestamp_ext
            .as_ext()
            .expect("First value in array should be an timestamp ext");

        let app_key = match &log_entry_value {
            rmpv::Value::Map(map) => map
                .iter()
                .filter(|(key, _)| key.as_str().expect("Key in map must be a string.") == APP_KEY)
                .map(|(_, value)| {
                    value
                        .as_str()
                        .expect(&format!("{} must be a string", APP_KEY))
                })
                .next()
                .unwrap_or("app_key_missing"),
            _ => "",
        };

        LogEntry {
            timestamp: LogEntry::read_msgpack_timestamp(ext, bytes).unwrap(),
            app_key: app_key.into(),
            log_entry_value,
        }
    }

    fn read_msgpack_timestamp(
        ext: i8,
        bytes: &[u8],
    ) -> Result<time::OffsetDateTime, TryFromSliceError> {
        assert_eq!(ext, 0);
        let (sec_bytes, nanos_bytes) = bytes.split_at(4);
        let secs: u32 = u32::from_be_bytes(sec_bytes.try_into()?);
        let nanos: u32 = u32::from_be_bytes(nanos_bytes.try_into()?);
        let timestamp =
            OffsetDateTime::from_unix_timestamp(secs as i64) + Duration::nanoseconds(nanos as i64);
        Ok(timestamp)
    }

    pub fn body(&self) -> &rmpv::Value {
        &self.log_entry_value
    }
}

impl TryFrom<Value> for LogEntry {
    type Error = anyhow::Error;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        let mut array = match value {
            rmpv::Value::Array(vec) => vec,
            val => anyhow::bail!("Value is not array: {}", val),
        };
        anyhow::ensure!(array.len() == 2, "Size of msgpack array must be 2.");
        let log_entry_value = array.pop().expect("Asserted size 2.");
        let timestamp_ext = array.pop().expect("Asserted size 2.");
        let (ext, bytes) = timestamp_ext
            .as_ext()
            .expect("First value in array should be an timestamp ext");

        let app_key = match &log_entry_value {
            rmpv::Value::Map(map) => map
                .iter()
                .filter(|(key, _)| key.as_str().expect("Key in map must be a string.") == APP_KEY)
                .map(|(_, value)| {
                    value
                        .as_str()
                        .expect(&format!("{} must be a string", APP_KEY))
                })
                .next()
                .unwrap_or("app_key_missing"),
            val => anyhow::bail!("Log entry value not a map: {}", val),
        };

        Ok(LogEntry {
            timestamp: LogEntry::read_msgpack_timestamp(ext, bytes).unwrap(),
            app_key: app_key.into(),
            log_entry_value,
        })
    }
}

use crate::block::BlockKey;
use crate::{AppKey, AppResult};
use itertools::Itertools;
use rocksdb::{Direction, IteratorMode, ReadOptions, WriteBatch, DB};
use std::path::Path;
use std::sync::Arc;
use time::OffsetDateTime;

#[derive(Clone)]
pub struct RocksIndex {
    db: Arc<DB>,
}

impl RocksIndex {
    pub fn new(path: impl AsRef<Path>) -> AppResult<RocksIndex> {
        let db = DB::open_default(path.as_ref())?;
        Ok(RocksIndex { db: Arc::new(db) })
    }

    pub fn new_temp_index() -> AppResult<RocksIndex> {
        let dir = tempfile::tempdir()?;
        Ok(RocksIndex::new(dir.path())?)
    }

    pub fn put(
        &self,
        min_time: &OffsetDateTime,
        max_time: &OffsetDateTime,
        block_key: &BlockKey,
    ) -> AppResult<()> {
        let app_key = block_key.app_key.clone();
        let mut batch = WriteBatch::default();

        let min_time_bytes = min_time.timestamp().to_be_bytes();
        batch.put(min_time_bytes, &block_key.to_string());
        batch.put(
            &Self::join_key(app_key.as_bytes(), &min_time_bytes),
            &block_key.to_string(),
        );

        let max_time_bytes = max_time.timestamp().to_be_bytes();
        batch.put(max_time_bytes, &block_key.to_string());
        batch.put(
            &Self::join_key(app_key.as_bytes(), &max_time_bytes),
            &block_key.to_string(),
        );

        Ok(self.db.write(batch)?)
    }

    fn join_key(app_key: &[u8], time_bytes: &[u8]) -> Vec<u8> {
        let mut buf = Vec::from(app_key);
        buf.push(b'/');
        buf.extend_from_slice(time_bytes);
        buf
    }

    pub fn fetch_range(
        &self,
        min_time: &OffsetDateTime,
        max_time: &OffsetDateTime,
    ) -> Vec<BlockKey> {
        let mut read_options = ReadOptions::default();
        // Need to add 1 because upper bound is non-inclusive
        read_options.set_iterate_upper_bound((max_time.timestamp() + 1).to_be_bytes());
        let db_iter = self.db.iterator_opt(
            IteratorMode::From(&min_time.timestamp().to_be_bytes(), Direction::Forward),
            read_options,
        );
        db_iter
            .map(|(_, block_key)| BlockKey::from_string(String::from_utf8_lossy(&block_key)))
            .unique()
            .collect()
    }

    pub fn fetch_app_range(
        &self,
        app_key: &AppKey,
        min_time: &OffsetDateTime,
        max_time: &OffsetDateTime,
    ) -> Vec<BlockKey> {
        let mut read_options = ReadOptions::default();
        // Need to add 1 because upper bound is non-inclusive
        let min_key = Self::join_key(app_key.as_bytes(), &min_time.timestamp().to_be_bytes());
        let max_key = Self::join_key(
            app_key.as_bytes(),
            &(max_time.timestamp() + 1).to_be_bytes(),
        );
        read_options.set_iterate_upper_bound(max_key);
        let db_iter = self.db.iterator_opt(
            IteratorMode::From(&min_key, Direction::Forward),
            read_options,
        );
        db_iter
            .map(|(_, block_key)| BlockKey::from_string(String::from_utf8_lossy(&block_key)))
            .unique()
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lazy_static::lazy_static;
    use time::Duration;

    lazy_static! {
        static ref APP_KEY: AppKey = "foo".into();
    }

    #[test]
    fn fetch_range_contains_key() {
        let block_min_time = OffsetDateTime::from_unix_timestamp(1611819345);
        let block_max_time = block_min_time + Duration::seconds(5);
        let index = new_index();
        let block_key = block_key(block_min_time, block_max_time);

        index
            .put(&block_min_time, &block_max_time, &block_key)
            .unwrap();

        let results = index.fetch_range(
            &(block_min_time - Duration::seconds(2)),
            &(block_max_time + Duration::seconds(2)),
        );
        assert_eq!(results.len(), 1);
        assert_eq!(results[0], block_key);
    }

    #[test]
    fn fetch_range_before_key() {
        let block_min_time = OffsetDateTime::from_unix_timestamp(1611819345);
        let block_max_time = block_min_time + Duration::seconds(5);
        let index = new_index();
        let block_key = block_key(block_min_time, block_max_time);

        index
            .put(&block_min_time, &block_max_time, &block_key)
            .unwrap();

        let results = index.fetch_range(
            &(block_min_time - Duration::seconds(5)),
            &(block_min_time - Duration::seconds(2)),
        );
        assert_eq!(results.len(), 0);
    }

    #[test]
    fn fetch_range_after_key() {
        let block_min_time = OffsetDateTime::from_unix_timestamp(1611819345);
        let block_max_time = block_min_time + Duration::seconds(5);
        let index = new_index();
        let block_key = block_key(block_min_time, block_max_time);

        index
            .put(&block_min_time, &block_max_time, &block_key)
            .unwrap();

        let results = index.fetch_range(
            &(block_max_time + Duration::seconds(2)),
            &(block_max_time + Duration::seconds(5)),
        );
        assert_eq!(results.len(), 0);
    }

    #[test]
    fn fetch_range_max_equals_key() {
        let block_min_time = OffsetDateTime::from_unix_timestamp(1611819345);
        let block_max_time = block_min_time + Duration::seconds(5);
        let index = new_index();
        let block_key = block_key(block_min_time, block_max_time);

        index
            .put(&block_min_time, &block_max_time, &block_key)
            .unwrap();

        let results = index.fetch_range(&(block_min_time - Duration::seconds(5)), &block_min_time);
        assert_eq!(results.len(), 1);
    }

    #[test]
    fn fetch_range_app_contains_key() {
        let block_min_time = OffsetDateTime::from_unix_timestamp(1611819345);
        let block_max_time = block_min_time + Duration::seconds(5);
        let index = new_index();
        let block_key = block_key(block_min_time, block_max_time);

        index
            .put(&block_min_time, &block_max_time, &block_key)
            .unwrap();

        let results = index.fetch_app_range(
            &APP_KEY,
            &(block_min_time - Duration::seconds(2)),
            &(block_max_time + Duration::seconds(2)),
        );
        assert_eq!(results.len(), 1);
        assert_eq!(results[0], block_key);
    }

    fn block_key(min_time: OffsetDateTime, max_time: OffsetDateTime) -> BlockKey {
        BlockKey {
            min_time,
            max_time,
            app_key: APP_KEY.clone(),
            header_size: 10,
        }
    }

    pub fn new_index() -> RocksIndex {
        RocksIndex::new_temp_index().unwrap()
    }
}

use std::collections::HashMap;
use std::io;
use std::sync::Arc;

use futures::StreamExt;
use kv_log_macro as logr;
use log::kv;
use rmpv::decode;
use tokio::sync::broadcast;
use tokio::sync::Mutex;

use crate::block::storage::BlockStore;
use crate::block::Block;
use crate::entry::LogEntry;
use crate::index::RocksIndex;
use crate::{AppResult, BoxedStream, LogEntryStreamer};
use std::convert::TryFrom;

#[derive(Clone)]
pub struct Ingestor {
    block_store: BlockStore,
    index: RocksIndex,
    block_map: Arc<Mutex<HashMap<String, Block>>>,
    min_block_size_bytes: u64,

    broadcast_send: broadcast::Sender<Arc<LogEntry>>,
}

impl Ingestor {
    pub fn new(block_store: BlockStore, index: RocksIndex) -> Ingestor {
        let (send, _) = broadcast::channel(10);
        Ingestor {
            block_store,
            index,
            block_map: Arc::new(Mutex::new(HashMap::new())),
            // For now default to 1MB block size
            min_block_size_bytes: 500_000,
            broadcast_send: send,
        }
    }

    fn broadcast_entry(&self, entry: LogEntry) {
        let arc_entry = Arc::new(entry);
        if self.broadcast_send.receiver_count() > 0 {
            self.broadcast_send.send(arc_entry).unwrap_or_else(|_| {
                logr::error!("ingest_broadcast_no_recipients");
                0
            });
        }
    }

    async fn flush_block(
        &self,
        app_key: String,
        block_map: &mut HashMap<String, Block>,
    ) -> AppResult<()> {
        let block = block_map
            .remove(&app_key)
            .expect("Entries in block should have the same app key.");
        // TODO: Handle case where program exits after putting block but before saving index,
        // leading to orphaned entries in the block store.
        let block_key = self.block_store.put_block(app_key, &block).await?;
        self.index
            .put(&block.min_time(), &block.max_time(), &block_key)?;
        Ok(())
    }

    async fn ingest_entry(&self, entry: LogEntry, bytes_read: u64) -> AppResult<()> {
        let app_key = entry.app_key.clone();
        let block_map = &mut self.block_map.lock().await;
        let block_size_bytes = {
            let block = block_map
                .entry(app_key.clone())
                .or_insert_with(|| Block::new(entry.timestamp));
            block.add_entry(entry, bytes_read);
            logr::debug!("Ingested entry", { num_bytes: bytes_read });
            block.size()
        };
        if block_size_bytes > self.min_block_size_bytes {
            logr::debug!("Uploading block", { num_bytes: block_size_bytes });
            self.flush_block(app_key.clone(), block_map).await?;
        }

        Ok(())
    }

    pub async fn ingest_bytes(&self, bytes: bytes::Bytes) -> AppResult<()> {
        let mut prev_position = 0;
        let mut bytes_reader = io::Cursor::new(bytes.clone());
        while let Ok(value) = decode::read_value(&mut bytes_reader) {
            let entry = match LogEntry::try_from(value) {
                Ok(entry) => entry,
                Err(err) => {
                    logr::error!("failed to parse value", {
                        error: kv::Value::from_display(&err)
                    });
                    continue;
                }
            };
            let bytes_read = bytes_reader.position() - prev_position;
            if let Err(err) = self.ingest_entry(entry.clone(), bytes_read).await {
                logr::error!("failed to ingest entry", {
                    error: kv::Value::from_display(&err)
                });
            }
            self.broadcast_entry(entry);

            prev_position = bytes_reader.position();
        }
        Ok(())
    }
}

impl LogEntryStreamer for Ingestor {
    type Item = Arc<LogEntry>;

    fn stream(&self) -> BoxedStream<Self::Item> {
        Box::new(self.broadcast_send.subscribe().map(|res| res.unwrap()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::block::storage::VecStorage;

    #[tokio::test]
    async fn ingest_bytes() {
        let storage = VecStorage::default();
        let block_store = BlockStore::new(Box::new(storage));
        let index = RocksIndex::new_temp_index().unwrap();
        let ingestor = Ingestor::new(block_store, index);

        // TODO: Write this test
    }
}

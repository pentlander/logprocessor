use futures::Stream;
use time::OffsetDateTime;

use crate::entry::LogEntry;

pub mod block;
pub mod entry;
pub mod index;
pub mod ingest;
pub mod query;

pub type AppResult<T> = Result<T, anyhow::Error>;

pub type AppKey = String;

#[derive(Copy, Clone, Debug)]
pub struct DateTimeRange(pub OffsetDateTime, pub OffsetDateTime);

pub type BoxedStream<T> = Box<dyn Stream<Item = T> + Unpin + Send>;

pub trait LogEntryStreamer: Send + Sync {
    type Item: AsRef<LogEntry>;

    fn stream(&self) -> BoxedStream<Self::Item>;
}

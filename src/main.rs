use bytes;
use s3::{bucket, creds, region};
use serde::Deserialize;
use time::OffsetDateTime;
use warp::{Filter, Rejection, Reply};

use futures::StreamExt;
use logprocessor::block::storage::{BlockStore, Storage, VecStorage};
use logprocessor::index::RocksIndex;
use logprocessor::ingest::Ingestor;
use logprocessor::query::Querier;
use logprocessor::{AppKey, DateTimeRange};
use std::convert::Infallible;
use warp::reject::Reject;

struct StorageInfo {
    region: region::Region,
    credentials: creds::Credentials,
    bucket: String,
}

#[derive(Deserialize)]
struct SearchQuery {
    q: String,
    min_time: String,
    max_time: String,
    app_key: Option<AppKey>,
}

fn with_ingestor(
    ingestor: Ingestor,
) -> impl Filter<Extract = (Ingestor,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || ingestor.clone())
}

fn with_querier(
    querier: Querier,
) -> impl Filter<Extract = (Querier,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || querier.clone())
}

async fn handle_ingest(
    bytes: bytes::Bytes,
    ingestor: Ingestor,
) -> Result<impl Reply, warp::Rejection> {
    let ingestor = ingestor.clone();
    &ingestor.ingest_bytes(bytes).await;

    Ok(warp::reply::with_status::<String>(
        "OK".to_string(),
        warp::http::StatusCode::OK,
    ))
}

#[derive(Debug)]
struct DateTimeParseError(time::ParseError);

impl Reject for DateTimeParseError {}

#[derive(Debug)]
struct AppError(anyhow::Error);

impl Reject for AppError {}

fn parse_datetime(datetime_str: &str) -> Result<OffsetDateTime, Rejection> {
    OffsetDateTime::parse(datetime_str, "%FT%T%z")
        .map_err(|e| warp::reject::custom(DateTimeParseError(e)))
}

async fn handle_query(query: SearchQuery, querier: Querier) -> Result<impl Reply, warp::Rejection> {
    let querier = querier.clone();
    let min_time = parse_datetime(&query.min_time)?;
    let max_time = parse_datetime(&query.max_time)?;
    let results = querier
        .query(
            query.app_key.as_ref(),
            DateTimeRange(min_time, max_time),
            query.q,
        )
        .await
        .map_err(|e| warp::reject::custom(AppError(e)))?;
    Ok(warp::reply::json(&results))
}

fn handle_tail(querier: Querier) -> impl Reply {
    let stream = querier
        .tail()
        .map(|log_entry| Ok::<_, Infallible>(warp::sse::json(log_entry)));
    warp::sse::reply(warp::sse::keep_alive().stream(stream))
}

fn local_bucket_storage() -> Box<dyn Storage> {
    let minio = StorageInfo {
        region: region::Region::Custom {
            region: "local".into(),
            endpoint: "http://127.0.0.1:9000".into(),
        },
        credentials: creds::Credentials {
            access_key: Some("AKIAIOSFODNN7EXAMPLE".into()),
            secret_key: Some("wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY".into()),
            security_token: None,
            session_token: None,
        },
        bucket: "test-logs".into(),
    };

    let bucket =
        bucket::Bucket::new_with_path_style(&minio.bucket, minio.region, minio.credentials)
            .unwrap();
    Box::new(bucket)
}

fn in_memory_storage() -> Box<dyn Storage> {
    Box::new(VecStorage::default())
}

#[tokio::main]
async fn main() {
    femme::with_level(log::LevelFilter::Debug);

    let storage = in_memory_storage();
    let block_store = BlockStore::new(storage);
    // let index = RocksIndex::new("./db").unwrap();
    let index = RocksIndex::new_temp_index().unwrap();
    let ingestor = Ingestor::new(block_store.clone(), index.clone());
    let querier = Querier::new(block_store, index, ingestor.clone());

    let log = warp::path!("ingest")
        .and(warp::body::bytes())
        .and(with_ingestor(ingestor))
        .and_then(handle_ingest);

    let query = warp::path!("query")
        .and(warp::query())
        .and(with_querier(querier.clone()))
        .and_then(handle_query);

    let tail = warp::path!("tail")
        .and(with_querier(querier))
        .map(handle_tail);

    warp::serve(log.or(query).or(tail))
        .run(([127, 0, 0, 1], 3030))
        .await;
}

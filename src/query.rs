use std::sync::Arc;

use futures::future;
use kv_log_macro as log;

use crate::block::storage::BlockStore;
use crate::entry::LogEntry;
use crate::index::RocksIndex;
use crate::{AppKey, AppResult, BoxedStream, DateTimeRange, LogEntryStreamer};

#[derive(Clone)]
pub struct Querier {
    block_store: BlockStore,
    index: RocksIndex,
    log_entry_streamer: Arc<dyn LogEntryStreamer<Item = Arc<LogEntry>>>,
}

impl Querier {
    pub fn new(
        block_store: BlockStore,
        index: RocksIndex,
        log_entry_streamer: impl LogEntryStreamer<Item = Arc<LogEntry>> + 'static,
    ) -> Querier {
        Querier {
            block_store,
            index,
            log_entry_streamer: Arc::new(log_entry_streamer),
        }
    }

    pub async fn query_range(
        &self,
        datetime_range: DateTimeRange,
        query: String,
    ) -> AppResult<Vec<LogEntry>> {
        self.query(None, datetime_range, query).await
    }

    pub async fn query_app_range(
        &self,
        app_key: &AppKey,
        datetime_range: DateTimeRange,
        query: String,
    ) -> AppResult<Vec<LogEntry>> {
        self.query(Some(app_key), datetime_range, query).await
    }

    pub async fn query(
        &self,
        app_key: Option<&AppKey>,
        DateTimeRange(min_time, max_time): DateTimeRange,
        query: String,
    ) -> AppResult<Vec<LogEntry>> {
        let block_keys = match app_key {
            Some(app_key) => self.index.fetch_app_range(app_key, &min_time, &max_time),
            None => self.index.fetch_range(&min_time, &max_time),
        };
        let block_results = future::join_all(
            block_keys
                .iter()
                .map(|block_key| self.block_store.get_block(block_key)),
        )
        .await;

        let query_regex = regex::Regex::new(&query)?;
        Ok(block_results
            .into_iter()
            .filter_map(|result| match result {
                Ok(block) => Some(block),
                Err(err) => {
                    log::error!("query_get_block_error: {}", err);
                    None
                }
            })
            .flat_map(|block| block.into_entries().into_iter())
            .skip_while(|entry| entry.timestamp < min_time)
            .take_while(|entry| entry.timestamp < max_time)
            .filter(|entry| {
                let message_pair = entry
                    .body()
                    .as_map()
                    .expect("Entry body should be a map")
                    .iter()
                    .find(|(key, _)| {
                        key.as_str().expect("Map keys should be strings") == "message"
                    });
                message_pair
                    .and_then(|(_, message)| message.as_str())
                    .and_then(|msg_str| query_regex.find(msg_str))
                    .is_some()
            })
            .collect())
    }

    pub fn tail(&self) -> BoxedStream<Arc<LogEntry>> {
        self.log_entry_streamer.as_ref().stream()
    }
}
